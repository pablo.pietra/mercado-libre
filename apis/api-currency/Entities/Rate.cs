using System.Runtime.Serialization;

namespace api_currency
{
    [DataContract(Name="rate")]
    public class Rate
    {
        [DataMember(Name="value")]
        public decimal Value { get; set; }
    }
}