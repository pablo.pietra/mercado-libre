using System.Collections.Generic;
using System.Threading.Tasks;

namespace api_currency
{
    public interface ICurrencyService
    {
        Task<Dictionary<string, decimal>> Get();
    }
}