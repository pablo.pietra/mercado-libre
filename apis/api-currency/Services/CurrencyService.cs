using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace api_currency
{
    public class CurrencyService : ICurrencyService
    {
        private string _key;
        private string _baseUrl;
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;

        public CurrencyService(HttpClient client, IConfiguration configuration) 
        {
            _client = client;
            _configuration = configuration;

            _key = configuration.GetSection("Key").Value;
            _baseUrl = configuration.GetSection("BaseUrl").Value;
        }

        public async Task<Dictionary<string, decimal>> Get()
        {
            var httpResponse = await _client.GetAsync($"{_baseUrl}?access_key={_key}");

            if (!httpResponse.IsSuccessStatusCode) throw new Exception("Cannot retrieve tasks");

            var content = await httpResponse.Content.ReadAsStringAsync();
        
            dynamic d = JsonConvert.DeserializeObject(content);

            return JsonConvert.DeserializeObject<Dictionary<string, decimal>>(d["rates"].ToString());
        }
    }
}