﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace api_currency.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CurrencyController : ControllerBase
    {
        public ICurrencyService _currencyService;

        public CurrencyController(ICurrencyService currencyService) => _currencyService = currencyService;

        [HttpGet("{from}/{to}")]
        public async Task<IActionResult> GetRate(string from, string to) 
        {
            try
            {
                var currencies = await _currencyService.Get();

                var rate = currencies[from.ToUpper()] / currencies[to.ToUpper()];

                return Ok(rate);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        } 
    }
}
