﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace api_geolocation.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class GeolocationController : ControllerBase
    {
        public IGeolocationService _geolocationService;

        public GeolocationController(IGeolocationService geolocationService) => _geolocationService = geolocationService;

        [HttpGet("{ip}")]
        public async Task<IActionResult> Get(string ip) 
        {
            try
            {
                var country = await _geolocationService.Get(ip);

                return Ok(country);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        } 
    }
}
