using System.Threading.Tasks;

namespace api_geolocation
{
    public interface IGeolocationService
    {
        Task<Country> Get(string ip);
    }
}