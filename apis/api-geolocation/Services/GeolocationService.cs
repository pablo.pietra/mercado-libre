using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace api_geolocation
{
    public class GeolocationService : IGeolocationService
    {
        private string _baseUrl;
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;

        public GeolocationService(HttpClient client, IConfiguration configuration) 
        {
            _client = client;
            _configuration = configuration;

            _baseUrl = configuration.GetSection("BaseUrl").Value;
        }

        public async Task<Country> Get(string ip)
        {
            var httpResponse = await _client.GetAsync($"{_baseUrl}{ip}");

            if (!httpResponse.IsSuccessStatusCode) throw new Exception("Cannot retrieve tasks");

            var content = await httpResponse.Content.ReadAsStringAsync();
            
            return JsonConvert.DeserializeObject<Country>(content);
        }
    }
}