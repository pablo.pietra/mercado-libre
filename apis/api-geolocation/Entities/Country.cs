using System.Runtime.Serialization;

namespace api_geolocation
{
    [DataContract(Name="country")]
    public class Country
    {
        [DataMember(Name="countryCode")]
        public string Code { get; set; }
        
        [DataMember(Name="countryName")]
        public string Name { get; set; }
    }
}