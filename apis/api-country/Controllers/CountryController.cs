﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace api_country.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CountryController : ControllerBase
    {
        public ICountryService _countryService;

        public CountryController(ICountryService countryService) => _countryService = countryService;

        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code) 
        {
            try
            {
                var country = await _countryService.Get(code);

                return Ok(country);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
