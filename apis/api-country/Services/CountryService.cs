using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace api_country
{
    public class CountryService : ICountryService
    {
        private string _baseUrl;
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;

        public CountryService(HttpClient client, IConfiguration configuration) 
        {
            _client = client;
            _configuration = configuration;

            _baseUrl = configuration.GetSection("BaseUrl").Value;
        }

        public async Task<Country> Get(string code)
        {
            var httpResponse = await _client.GetAsync($"{_baseUrl}{code}");

            if (!httpResponse.IsSuccessStatusCode) throw new Exception("Cannot retrieve tasks");

            var content = await httpResponse.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Country>(content);
        }
    }
}