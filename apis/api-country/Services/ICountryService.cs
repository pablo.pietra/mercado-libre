using System.Threading.Tasks;

namespace api_country
{
    public interface ICountryService
    {
        Task<Country> Get(string code);
    }
}