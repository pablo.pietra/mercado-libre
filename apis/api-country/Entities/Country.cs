using System.Collections.Generic;
using System.Runtime.Serialization;

namespace api_country
{
    [DataContract(Name="country")]
    public class Country
    {
        [DataMember(Name="alpha2Code")]
        public string Code { get; set; }
        
        [DataMember(Name="name")]
        public string Name { get; set; }

        [DataMember(Name="currencies")]
        public List<Currency> Currencies { get; set; }

        [DataMember(Name="timezones")]
        public List<string> TimeZones { get; set; }

        [DataMember(Name="languages")]
        public List<Language> Languages { get; set; }

        [DataMember(Name="latlng")]
        public List<decimal> Coordinates { get; set; }
    }
}