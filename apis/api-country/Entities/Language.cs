using System.Runtime.Serialization;

namespace api_country
{
    [DataContract(Name="language")]
    public class Language
    {
        [DataMember(Name="iso639_1")]
        public string Code { get; set; }
        
        [DataMember(Name="name")]
        public string Name { get; set; }
    }
}