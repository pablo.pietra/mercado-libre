﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace app.Controllers
{
    [ApiController]
    [EnableCors("AllowAll")]
    [Route("[controller]/[action]")]
    public class AppController : ControllerBase
    {

        private readonly IAppService _appService;

        public AppController(IAppService appService)
        {
            _appService = appService;
        }

        [HttpGet("{ip}")]
        public async Task<IActionResult> Get(string ip)
        {
            try
            {
                Validate(ip);

                return Ok(await _appService.GetCountryByIp(ip));
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetStats()
        {
            try
            {
                return Ok(await _appService.GetStats());
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private void Validate(string ip)
        {
            IPAddress result;
    
            if (string.IsNullOrEmpty(ip) || !IPAddress.TryParse(ip, out result)) throw new System.Exception("Formato de IP invalido.");
        }
    }
}
