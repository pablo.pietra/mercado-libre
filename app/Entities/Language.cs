using System.Runtime.Serialization;

namespace app
{
    [DataContract(Name="language")]
    public class Language
    {
        [DataMember(Name="Code")]
        public string Code { get; set; }
        
        [DataMember(Name="Name")]
        public string Name { get; set; }
    }
}