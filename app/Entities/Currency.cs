using System.Runtime.Serialization;

namespace app
{
    [DataContract(Name="currency")]
    public class Currency
    {
       [DataMember(Name="code")]
        public string Code { get; set; }
        
        [DataMember(Name="name")]
        public string Name { get; set; }

        [DataMember(Name="symbol")]
        public string Symbol { get; set; }

        [DataMember(Name="rate")]
        public decimal Rate { get; set; }
    }
}