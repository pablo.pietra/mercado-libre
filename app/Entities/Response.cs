using System.Runtime.Serialization;

namespace app
{
    [DataContract(Name="response")]
    public class Response
    {
        [DataMember(Name="Country")]
        public Country Country { get; set; }
        
        [DataMember(Name="Distance")]
        public double Distance { get; set; }
    }
}