using System.Runtime.Serialization;

namespace app
{
    [DataContract(Name="stats")]
    public class Stats
    {
        [DataMember(Name="country")]
        public Country Country { get; set; }
        
        [DataMember(Name="distance")]
        public double Distance { get; set; }

        [DataMember(Name="hits")]
        public int Hits { get; set; }
    }
}