using System.Runtime.Serialization;

namespace app
{
    [DataContract(Name="responseStats")]
    public class ResponseStats
    {
        [DataMember(Name="minDistance")]
        public double MinDistance { get; set; }
        
        [DataMember(Name="maxDistance")]
        public double MaxDistance { get; set; }

        [DataMember(Name="avgDistance")]
        public double AvgDistance { get; set; }
    }
}