using System.Collections.Generic;
using System.Runtime.Serialization;

namespace app
{
    [DataContract(Name="country")]
    public class Country
    {
        [DataMember(Name="Code")]
        public string Code { get; set; }
        
        [DataMember(Name="Name")]
        public string Name { get; set; }

        [DataMember(Name="currencies")]
        public List<Currency> Currencies { get; set; }

        [DataMember(Name="TimeZones")]
        public List<string> TimeZones { get; set; }

        [DataMember(Name="Languages")]
        public List<Language> Languages { get; set; }

        [DataMember(Name="coordinates")]
        public List<double> Coordinates { get; set; }
    }
}