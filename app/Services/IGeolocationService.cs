using System.Threading.Tasks;

namespace app
{
    public interface IGeolocationService
    {
        Task<Country> Get(string ip);
    }
}