using System.Threading.Tasks;

namespace app
{
    public interface ICountryService
    {
        Task<Country> Get(string code);
    }
}