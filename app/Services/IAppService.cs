using System.Threading.Tasks;

namespace app
{
    public interface IAppService
    {
        Task<Response> GetCountryByIp(string ip);

        Task<ResponseStats> GetStats();
    }
}