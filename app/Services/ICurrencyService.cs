using System.Threading.Tasks;

namespace app
{
    public interface ICurrencyService
    {
        Task<decimal> GetRate(string code);
    }
}