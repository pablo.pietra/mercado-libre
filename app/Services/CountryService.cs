using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace app
{
    public class CountryService : ICountryService
    {
        private readonly string _baseUrl;
        private readonly IClient _client;
        private readonly IConfiguration _configuration;

        public CountryService(IClient client, IConfiguration configuration) 
        {
            _client = client;
            _configuration = configuration;

            _baseUrl = configuration.GetSection("ApiCountry").GetSection("BaseUrl").Value;
        }

        public async Task<Country> Get(string code) => await _client.Get<Country>($"{_baseUrl}/{code}");
    }
}