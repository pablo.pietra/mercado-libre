using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace app
{
    public class GeolocationService : IGeolocationService
    {
        private readonly string _baseUrl;
        private readonly IClient _client;
        private readonly IConfiguration _configuration;

        public GeolocationService(IClient client, IConfiguration configuration) 
        {
            _client = client;
            _configuration = configuration;

            _baseUrl = configuration.GetSection("ApiGeolocation").GetSection("BaseUrl").Value;
        }

        public async Task<Country> Get(string ip) => await _client.Get<Country>($"{_baseUrl}/{ip}");
    }
}