using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace app
{
    public class CurrencyService : ICurrencyService
    {
        private readonly string _baseCode;
        private readonly string _baseUrl;
        private readonly IClient _client;
        private readonly IConfiguration _configuration;

        public CurrencyService(IClient client, IConfiguration configuration) 
        {
            _client = client;
            _configuration = configuration;

            _baseUrl = configuration.GetSection("ApiCurrency").GetSection("BaseUrl").Value;
            _baseCode = configuration.GetSection("ApiCurrency").GetSection("BaseCode").Value;
        }

        public async Task<decimal> GetRate(string code) => await _client.Get<decimal>($"{_baseUrl}/{_baseCode}/{code}");
    }
}