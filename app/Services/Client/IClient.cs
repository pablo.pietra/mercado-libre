using System.Threading.Tasks;

namespace app
{
    public interface IClient
    {
        Task<R> Get<R>(string url);
    }
}