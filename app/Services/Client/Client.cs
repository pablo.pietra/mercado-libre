using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace app
{
    public class Client : IClient
    {
        private readonly HttpClient _client;

        public Client(HttpClient client)
        {
            _client = client;
        }

        public async Task<R> Get<R>(string url)
        {
            var httpResponse = await _client.GetAsync(url);

            if (!httpResponse.IsSuccessStatusCode) throw new System.Exception("Cannot retrieve tasks");

            var content = await httpResponse.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<R>(content);
        }
    }
}