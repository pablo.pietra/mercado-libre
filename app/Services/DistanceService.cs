using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;

namespace app
{
    public class DistanceService : IDistanceService
    {
        private (double Latitude, double Longitude) _origin;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public DistanceService(IMemoryCache memoryCache, IConfiguration configuration) 
        {
            _memoryCache = memoryCache;
            _configuration = configuration;

            _origin = (double.Parse(configuration.GetSection("Origin:Latitude").Value), 
                       double.Parse(configuration.GetSection("Origin:Longitude").Value));
        }

        public double Calculate(Country country)
        {
            Stats stats;
            double distance;

            if (_memoryCache.TryGetValue($"stats_{country.Code}", out stats))
            {
                stats.Hits++;

                distance = stats.Distance;
            }
            else
            {
                var d1 = (_origin.Latitude) * (Math.PI / 180.0);
                var num1 = (_origin.Longitude) * (Math.PI / 180.0);
                var d2 = country.Coordinates[0] * (Math.PI / 180.0);
                var num2 = country.Coordinates[1] * (Math.PI / 180.0) - num1;
                var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

                distance = 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3))) / 1000;

                stats = new Stats
                {
                    Country = country,
                    Distance = distance,
                    Hits = 1,
                };

                TrackStatsKey($"stats_{country.Code}");
            }

            _memoryCache.Set($"stats_{country.Code}", stats);

            return distance;
        }

        public void Evaluate(Country country, double distance)
        {
            Evaluate(distance, "minDistance", (x, y) => x < y);
            Evaluate(distance, "maxDistance", (x, y) => x > y);
        }

        public double GetMin()
        {
            double min = 0;
            _memoryCache.TryGetValue("minDistance", out min);

            return min;
        }

        public double GetMax()
        {
            double max = 0;
            _memoryCache.TryGetValue("maxDistance", out max);

            return max;
        }

        public double CalculateAvg()
        {
            var sum = 0.0;
            var totalHits = 0;

            List<string> statsKeys;

            if (!_memoryCache.TryGetValue("statsKeys", out statsKeys)) return sum;

            foreach(var key in statsKeys) 
            {
                var stats = _memoryCache.Get<Stats>(key);

                sum += stats.Distance * stats.Hits;

                totalHits += stats.Hits;
            }

            return sum / totalHits;
        }

        private void Evaluate(double distance, string key, Func<double, double, bool> evaluation)
        {
            double value;

            if (_memoryCache.TryGetValue(key, out value))
            {
                if (evaluation(distance, value)) 
                    _memoryCache.Set(key, distance);
            }
            else
            {
                _memoryCache.Set(key, distance);
            }
        }

        private void TrackStatsKey(string key) 
        {
            List<string> statsKeys;

            if (!_memoryCache.TryGetValue("statsKeys", out statsKeys)) statsKeys = new List<string>();

            statsKeys.Add(key);

            _memoryCache.Set("statsKeys", statsKeys);
        }
    }
}