
namespace app
{
    public interface IDistanceService
    {
        double Calculate(Country country);

        void Evaluate(Country country, double distance);

        double GetMin();

        double GetMax();

        double CalculateAvg();
    }
}