using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

namespace app
{
    public class AppService : IAppService
    {
        private readonly IMemoryCache _memoryCache;
        private readonly ICountryService _countryService;
        private readonly ICurrencyService _currencyService;
        private readonly IDistanceService _distanceService;
        private readonly IGeolocationService _geolocationService;

        public AppService(IMemoryCache memoryCache, 
                          ICountryService countryService, 
                          ICurrencyService currencyService,
                          IDistanceService distanceService,
                          IGeolocationService geolocationService) 
        {
            _memoryCache = memoryCache;
            _countryService = countryService;
            _currencyService = currencyService;
            _distanceService = distanceService;
            _geolocationService = geolocationService;
        }

        public async Task<Response> GetCountryByIp(string ip)
        {
            var code = await GetCode(ip);

            var country = await GetCountry(code);

            var distance = _distanceService.Calculate(country); 
            
            await Task.Run(() => _distanceService.Evaluate(country, distance));

            return new Response
            {
                Country = country,
                Distance = distance,
            };
        }

        public async Task<ResponseStats> GetStats()
        {
            var taskGetMin = Task.Run(() => _distanceService.GetMin());
            var taskGetMax = Task.Run(() => _distanceService.GetMax());
            var taskGetAvg = Task.Run(() => _distanceService.CalculateAvg());

            await Task.WhenAll(taskGetMin, taskGetMax, taskGetAvg);

            return new ResponseStats
            {
                MinDistance = taskGetMin.Result,
                MaxDistance = taskGetMax.Result,
                AvgDistance = taskGetAvg.Result,
            };
        }

        private async Task<Country> GetCountry(string code) 
        {
            Country country;

            if (!_memoryCache.TryGetValue($"country_{code}", out country))
            {
                country =  await _countryService.Get(code);

                _memoryCache.Set($"country_{code}", country, DateTime.Now.AddDays(1));
            }

            foreach (var currency in country.Currencies) currency.Rate = await GetRate(currency.Code);

            return country;
        }

        
        private async Task<decimal> GetRate(string code)
        {
            decimal rate;

            if (!_memoryCache.TryGetValue($"rate_{code}", out rate))
            {
                rate =  await _currencyService.GetRate(code);

                _memoryCache.Set($"rate_{code}", rate, DateTime.Now.AddHours(1));
            }

            return rate;
        } 

        private async Task<string> GetCode(string ip) => (await _geolocationService.Get(ip)).Code;
    }
}